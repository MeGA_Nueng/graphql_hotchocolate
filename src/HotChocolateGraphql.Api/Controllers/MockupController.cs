﻿using HotChocolateGraphql.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;
using System.Net;

namespace HotChocolateGraphql.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class MockupController : Controller
{
    private readonly IRollService _rollService;

    public MockupController(IRollService rollService) => _rollService = rollService;

    [HttpGet("AddressData")]
    [SwaggerOperation("Insert Data Mockup (Override)")]
    public async Task<IActionResult> GetAddressData()
    {
        return Ok( await _rollService.AddressDataMockup());
    }

    [HttpGet("EmployeeData")]
    [SwaggerResponse((int)HttpStatusCode.OK, "if(Data in Table:[Employees] < 100 rows)? Insert Data Mockup", typeof(int))]
    [SwaggerOperation("Insert Data Mockup: 500,000 rows")]
    public async Task<IActionResult> GetEmployeeData()
    {
        return Ok( await _rollService.EmployeeDataMockup());
    }
}
