﻿using HotChocolateGraphql.Api.Context;
using HotChocolateGraphql.Api.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace HotChocolateGraphql.Api.Repositories;
public class EmployeeRepository : IEmployeeRepository
{
    readonly DbSet<Employee> _employees;

    public EmployeeRepository(DatabaseContext context)
    {
        _employees = context.Employees;
    }

    public IQueryable<Employee> GetAll() => _employees.Include(x => x.Roll).ThenInclude(x => x.Permission).Include(x => x.Address).AsQueryable();

    public IQueryable<Employee> Get(QueryEmployee where, string searchText, int? page = null, int per = 2000)
    {
        var query = _employees.Include(x => x.Roll).ThenInclude(x => x.Permission).AsQueryable();

        if (where?.RollId != null)
            query = query.Where(x => x.RollId.Equals(where.RollId));

        if (!string.IsNullOrWhiteSpace(where?.Name))
            query = query.Where(x => x.Name.Equals(where.Name));

        if (!string.IsNullOrWhiteSpace(searchText))
        {
            Expression<Func<Employee, bool>> explore = (x => x.Name.ToLower().Contains(searchText.ToLower()));

            query = query.Where(explore);
        }

        return query;
    }

    public Employee CreateEmp(CreateEmployeeInput employee)
    {
        var emp = new Employee()
        {
            Id = _employees.Max(x => x.Id) + 1,
            Name = employee.Name,
            RollId = employee.RollId,
        };

        _employees.Add(emp).Context.SaveChanges();

        return emp;
    }

    public bool DeleteEmp(DeleteEmployeeInput employee) => _employees.Where(x => x.Id == employee.Id).ExecuteDelete() > 0;
}
