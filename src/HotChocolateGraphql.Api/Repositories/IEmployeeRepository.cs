﻿using HotChocolateGraphql.Api.Models;

namespace HotChocolateGraphql.Api.Repositories;
public interface IEmployeeRepository
{
    IQueryable<Employee> GetAll();
    IQueryable<Employee> Get(QueryEmployee where, string searchText, int? page = null, int per = 2000);
    Employee CreateEmp(CreateEmployeeInput employee);
    bool DeleteEmp(DeleteEmployeeInput employee);
}
