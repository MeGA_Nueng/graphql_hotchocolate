using HotChocolateGraphql.Api.Context;
using HotChocolateGraphql.Api.GraphQL;
using HotChocolateGraphql.Api.Repositories;
using HotChocolateGraphql.Api.Services;
using HotChocolate.AspNetCore;
using HotChocolate.AspNetCore.Playground;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddAuthorization(
    option =>
    {
        option.AddPolicy("Admin", policy => policy.RequireClaim("role", "admin"));
        option.AddPolicy("Editor", policy => policy.RequireClaim("role", "editor"));
        option.AddPolicy("Api", policy => policy.RequireClaim("permission", "sample.api"));
    });

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.EnableAnnotations();
});

builder.Services.AddDbContext<DatabaseContext>(option => option.UseNpgsql(builder.Configuration.GetConnectionString("Main")));

var signingKey = new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes("key"));

builder.Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters =
            new TokenValidationParameters
            {
                ValidIssuer = "TCRB",
                ValidAudience = "TCRB",
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey
            };
    });

builder.Services.AddScoped<Query>();
builder.Services.AddScoped<Mutation>();
builder.Services.AddScoped<IEmployeeService, EmployeeService>();
builder.Services.AddScoped<IRollService, RollService>();
builder.Services.AddScoped<IEmployeeRepository, EmployeeRepository>();

builder.Services.AddGraphQLServer()
    .AddFiltering()
    .AddSorting()
    .AddQueryType<Query>()
    .AddMutationType<Mutation>()
    .AddAuthorization()
    .AddHttpRequestInterceptor<HttpRequestInterceptor>();

var app = builder.Build();

await using var scope = app.Services.CreateAsyncScope();
using var db = scope.ServiceProvider.GetService<DatabaseContext>();
await db.Database.MigrateAsync();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UsePlayground(new PlaygroundOptions
    {
        QueryPath = "/api",
        Path = "/playground"
    });
}

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(e =>
   {
       e.MapGraphQL("/api");
   });

app.UseHttpsRedirection();

app.MapControllers();

app.Run();

/// <summary>
/// Get Request Header => Authorization
/// </summary>
public class HttpRequestInterceptor : DefaultHttpRequestInterceptor
{
    public override ValueTask OnCreateAsync(HttpContext context,
        IRequestExecutor requestExecutor, IQueryRequestBuilder requestBuilder,
        CancellationToken cancellationToken)
    {
        var handler = new JwtSecurityTokenHandler();
        string authHeader = context.Request.Headers["Authorization"];
        if (authHeader != null)
        {
            authHeader = authHeader.Replace("Bearer ", "");
            var jsonToken = handler.ReadToken(authHeader);
            var tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
            var role = tokenS.Claims.First(claim => claim.Type == "role").Value;
            var permissionList = tokenS.Claims.ToList();
            var permission = tokenS.Claims.Where(c => c.Type.Equals("permission")).AsEnumerable();


            var identity = new ClaimsIdentity();
            identity.AddClaim(new Claim("role", role));
            identity.AddClaim(new Claim("permission", JsonConvert.SerializeObject(permission.ToArray())));

            context.User.AddIdentity(identity);
        }

        return base.OnCreateAsync(context, requestExecutor, requestBuilder,
            cancellationToken);
    }
}