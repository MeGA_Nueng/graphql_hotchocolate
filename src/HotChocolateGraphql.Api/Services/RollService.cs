﻿using HotChocolateGraphql.Api.Context;
using HotChocolateGraphql.Api.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace HotChocolateGraphql.Api.Services;
public class RollService : IRollService
{
    private readonly DatabaseContext _context;
    private readonly IHostEnvironment _environment;

    public RollService(DatabaseContext context, IHostEnvironment environment)
    {
        _environment = environment;
        _context = context;
    }

    public async Task<int> AddressDataMockup()
    {
        List<Address> Data;
        var filePath = System.IO.Path.Combine(_environment.ContentRootPath, "Mockup/address.json");
        using (var fs = File.OpenRead(filePath))
        {
            var decodeObject = JsonSerializer.Deserialize<List<Address>>(fs,
                                                                         new JsonSerializerOptions()
                                                                         {
                                                                             PropertyNameCaseInsensitive = true
                                                                         });
            Data = decodeObject.OrderBy(x => x.AddressId).ToList();
        }
        var oldData = await EntityFrameworkQueryableExtensions.ToListAsync(_context.Address);
        if (oldData != null)
            _context.Address.RemoveRange(oldData);
        await _context.Address.AddRangeAsync(Data);
        return await _context.SaveChangesAsync();
    }

    public async Task<int> EmployeeDataMockup()
    {
        List<Employee> Data;
        var filePath = System.IO.Path.Combine(_environment.ContentRootPath, "Mockup/EmployeesData.json");
        using (var fs = File.OpenRead(filePath))
        {
            var decodeObject = JsonSerializer.Deserialize<List<Employee>>(fs,
                                                                         new JsonSerializerOptions()
                                                                         {
                                                                             PropertyNameCaseInsensitive = true
                                                                         });
            Data = decodeObject.OrderBy(x => x.Id).ToList();
        }
        var oldData = await EntityFrameworkQueryableExtensions.ToListAsync(_context.Employees);
        if (oldData.Count() < 100)
        {
            _context.Employees.RemoveRange(oldData);
            await _context.Employees.AddRangeAsync(Data);
            return await _context.SaveChangesAsync();
        }
        else
            return 0;
    }

    public IQueryable<Roll> GetAll() => _context.Rolls.Include(x => x.Permission).AsQueryable();

}

