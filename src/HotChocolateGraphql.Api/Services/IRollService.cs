﻿using HotChocolateGraphql.Api.Models;

namespace HotChocolateGraphql.Api.Services;
public interface IRollService
{
    Task<int> AddressDataMockup();
    Task<int> EmployeeDataMockup();
    IQueryable<Roll> GetAll();
}

