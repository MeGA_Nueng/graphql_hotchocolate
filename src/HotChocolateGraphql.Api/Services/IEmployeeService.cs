﻿using HotChocolateGraphql.Api.Models;

namespace HotChocolateGraphql.Api.Services;
public interface IEmployeeService
{
    IQueryable<Employee> GetAll();
    IQueryable<Employee> Get(QueryEmployee where, string searchText, int? page = null, int per = 2000);

    Employee Create(CreateEmployeeInput employeeInput);

    bool Delete(DeleteEmployeeInput employeeInput);
}

