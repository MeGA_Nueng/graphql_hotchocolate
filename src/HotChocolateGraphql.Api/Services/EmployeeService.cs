﻿using HotChocolateGraphql.Api.Helper;
using HotChocolateGraphql.Api.Models;
using HotChocolateGraphql.Api.Repositories;

namespace HotChocolateGraphql.Api.Services;
public class EmployeeService : IEmployeeService
{
    private readonly IEmployeeRepository _employeeRepository;

    public EmployeeService(IEmployeeRepository employeeRepository)
    {
        _employeeRepository = employeeRepository;
    }

    public Employee Create(CreateEmployeeInput employeeInput) => _employeeRepository.CreateEmp(employeeInput);

    public bool Delete(DeleteEmployeeInput employeeInput) => _employeeRepository.DeleteEmp(employeeInput);

    public IQueryable<Employee> Get(QueryEmployee where, string searchText, int? page = null, int per = 2000) => _employeeRepository.Get(where, searchText, page, per);

    public IQueryable<Employee> GetAll() => _employeeRepository.GetAll();
}
