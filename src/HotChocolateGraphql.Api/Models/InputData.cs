﻿namespace HotChocolateGraphql.Api.Models
{
    public class CreateEmployeeInput
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; }
        public int RollId { get; set; }
        public int AddressId { get; set; }
    }

    public class DeleteEmployeeInput
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int? RollId { get; set; }
    }

    public class QueryEmployee
    {
        public string? Name { get; set; }
        public int? RollId { get; set; }
    }
}
