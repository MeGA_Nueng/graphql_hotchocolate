﻿using HotChocolate.Authorization;

namespace HotChocolateGraphql.Api.Models
{
    public class Employee
    {
        [Authorize(Policy = "Admin")]
        public int Id { get; set; }
        public string? Name { get; set; }
        public int? RollId { get; set; }
        public Roll? Roll { get; set; }
        public int? AddressId { get; set; }
        public Address? Address { get; set; }
    }

    public class Roll
    {
        public int RollId { get; set; }
        public string? RollName { get; set; }
        public int? PermissionId { get; set; }
        public Permission? Permission { get; set; }
    }

    public class Permission
    {
        [Authorize(Policy = "Editor")]
        public int PermissionId { get; set; }
        public string PermissionValue { get; set; }
    }

    public class Address
    {
        public int AddressId { get; set; }
        public string SubDistrict { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Zipcode { get; set; }
    }
}
