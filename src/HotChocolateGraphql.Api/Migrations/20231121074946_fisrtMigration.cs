﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace HotChocolateGraphql.Api.Migrations
{
    /// <inheritdoc />
    public partial class fisrtMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    AddressId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SubDistrict = table.Column<string>(type: "text", nullable: false),
                    District = table.Column<string>(type: "text", nullable: false),
                    Province = table.Column<string>(type: "text", nullable: false),
                    Zipcode = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.AddressId);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    PermissionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PermissionValue = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.PermissionId);
                });

            migrationBuilder.CreateTable(
                name: "Rolls",
                columns: table => new
                {
                    RollId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RollName = table.Column<string>(type: "text", nullable: true),
                    PermissionId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rolls", x => x.RollId);
                    table.ForeignKey(
                        name: "FK_Rolls_Permissions_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "Permissions",
                        principalColumn: "PermissionId");
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: true),
                    RollId = table.Column<int>(type: "integer", nullable: true),
                    AddressId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Address_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Address",
                        principalColumn: "AddressId");
                    table.ForeignKey(
                        name: "FK_Employees_Rolls_RollId",
                        column: x => x.RollId,
                        principalTable: "Rolls",
                        principalColumn: "RollId");
                });

            migrationBuilder.InsertData(
                table: "Address",
                columns: new[] { "AddressId", "District", "Province", "SubDistrict", "Zipcode" },
                values: new object[,]
                {
                    { 1, "คลองท่อม", "กระบี่", "คลองท่อมเหนือ", "81120" },
                    { 2, "คลองท่อม", "กระบี่", "คลองท่อมใต้", "81120" },
                    { 3, "คลองท่อม", "กระบี่", "คลองพน", "81170" },
                    { 4, "คลองท่อม", "กระบี่", "ทรายขาว", "81170" },
                    { 5, "คลองท่อม", "กระบี่", "พรุดินนา", "81120" }
                });

            migrationBuilder.InsertData(
                table: "Permissions",
                columns: new[] { "PermissionId", "PermissionValue" },
                values: new object[,]
                {
                    { 1, "CREATE,EDIT,DELETE" },
                    { 2, "EDIT" },
                    { 3, "" }
                });

            migrationBuilder.InsertData(
                table: "Rolls",
                columns: new[] { "RollId", "PermissionId", "RollName" },
                values: new object[,]
                {
                    { 1, 1, "Admin" },
                    { 2, 2, "Editor" },
                    { 3, 3, "Viewer" }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AddressId", "Name", "RollId" },
                values: new object[,]
                {
                    { 1, 1, "Anu1", 1 },
                    { 2, 2, "Anu2", 2 },
                    { 3, 3, "Anu3", 3 },
                    { 4, 4, "Anu4", 3 },
                    { 5, 5, "Anu5", 2 },
                    { 6, 3, "Anu6", 2 },
                    { 7, 2, "Anu7", 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_AddressId",
                table: "Employees",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_RollId",
                table: "Employees",
                column: "RollId");

            migrationBuilder.CreateIndex(
                name: "IX_Rolls_PermissionId",
                table: "Rolls",
                column: "PermissionId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "Rolls");

            migrationBuilder.DropTable(
                name: "Permissions");
        }
    }
}
