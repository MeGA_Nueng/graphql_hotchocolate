﻿using HotChocolateGraphql.Api.Models;
using HotChocolateGraphql.Api.Services;
using HotChocolate.Authorization;

namespace HotChocolateGraphql.Api.GraphQL;
public class Mutation
{
    private readonly IEmployeeService _employeeService;

    public Mutation(IEmployeeService employeeService)
    {
        _employeeService = employeeService;
    }

    [Authorize(Policy = "Admin")]
    public Employee CreateEmp(CreateEmployeeInput createEmployeeInput) => _employeeService.Create(createEmployeeInput);


    [Authorize(Policy = "Editor")]
    public bool DeleteEmp(DeleteEmployeeInput deleteEmployeeInput) => _employeeService.Delete(deleteEmployeeInput);
}
