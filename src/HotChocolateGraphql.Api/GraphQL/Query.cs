﻿using HotChocolateGraphql.Api.Models;
using HotChocolateGraphql.Api.Services;

namespace HotChocolateGraphql.Api.GraphQL;
public class Query
{
    private readonly IEmployeeService _employeeService;
    private readonly IRollService _rollService;

    public Query(IEmployeeService employeeService, IRollService rollService)
    {
        _employeeService = employeeService;
        _rollService = rollService;
    }

    public IQueryable<Roll> rolls => _rollService.GetAll();

    [UsePaging(MaxPageSize = 100)]
    [UseFiltering]
    [UseSorting]
    public IQueryable<Employee> employees => _employeeService.GetAll();
}
