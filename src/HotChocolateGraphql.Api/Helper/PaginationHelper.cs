﻿using HotChocolateGraphql.Api.Models;
using System.Net;

namespace HotChocolateGraphql.Api.Helper
{
    public static class PaginationHelper
    {
        public static IQueryable<Employee> UsePaging(this IQueryable<Employee> query, int? page = null, int per = 2000) =>
            page.HasValue ? query.Skip((page.Value - 1) * per).Take(per) : query;
    }
}
