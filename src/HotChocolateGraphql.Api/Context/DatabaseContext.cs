﻿using HotChocolateGraphql.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace HotChocolateGraphql.Api.Context;
public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    public DbSet<Employee> Employees { get; set; }
    public DbSet<Roll> Rolls { get; set; }
    public DbSet<Permission> Permissions { get; set; }
    public DbSet<Address> Address { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Employee>().HasData(
            new Employee()
            {
                Id = 1,
                Name = "Anu1",
                RollId = 1,
                AddressId = 1,
            },

            new Employee()
            {
                Id = 2,
                Name = "Anu2",
                RollId = 2,
                AddressId = 2,
            },

            new Employee()
            {
                Id = 3,
                Name = "Anu3",
                RollId = 3,
                AddressId = 3,
            },
            new Employee()
            {
                Id = 4,
                Name = "Anu4",
                RollId = 3,
                AddressId = 4,
            },
            new Employee()
            {
                Id = 5,
                Name = "Anu5",
                RollId = 2,
                AddressId = 5,
            },
            new Employee()
            {
                Id = 6,
                Name = "Anu6",
                RollId = 2,
                AddressId = 3,
            },
            new Employee()
            {
                Id = 7,
                Name = "Anu7",
                RollId = 3,
                AddressId = 2,
            }
            );
        modelBuilder.Entity<Roll>().HasData(
            new Roll()
            {
                RollId = 1,
                RollName = "Admin",
                PermissionId = 1,
            },
            new Roll()
            {
                RollId = 2,
                RollName = "Editor",
                PermissionId = 2,
            },
            new Roll()
            {
                RollId = 3,
                RollName = "Viewer",
                PermissionId = 3,
            }
            );
        modelBuilder.Entity<Permission>().HasData(
            new Permission()
            {
                PermissionId = 1,
                PermissionValue = "CREATE,EDIT,DELETE"
            },
            new Permission()
            {
                PermissionId = 2,
                PermissionValue = "EDIT"
            },
            new Permission()
            {
                PermissionId = 3,
                PermissionValue = ""
            }
            );
        modelBuilder.Entity<Address>().HasData(
            new Address()
            {
                AddressId = 1,
                SubDistrict = "คลองท่อมเหนือ",
                District = "คลองท่อม",
                Province = "กระบี่",
                Zipcode = "81120"
            },
            new Address()
            {
                AddressId = 2,
                SubDistrict = "คลองท่อมใต้",
                District = "คลองท่อม",
                Province = "กระบี่",
                Zipcode = "81120"
            },
            new Address()
            {
                AddressId = 3,
                SubDistrict = "คลองพน",
                District = "คลองท่อม",
                Province = "กระบี่",
                Zipcode = "81170"
            },
            new Address()
            {
                AddressId = 4,
                SubDistrict = "ทรายขาว",
                District = "คลองท่อม",
                Province = "กระบี่",
                Zipcode = "81170"
            },
            new Address()
            {
                AddressId = 5,
                SubDistrict = "พรุดินนา",
                District = "คลองท่อม",
                Province = "กระบี่",
                Zipcode = "81120"
            }
            );
    }
}